"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.
"""

print("""!Textová hra!
!Můj výtvor!
""")

def start():
    print ("""Jsi profesionální hráč volejbalu a je pátek den před zápasem.
Co budeš dělat večer?(Možné více odpovědí).

    a = půjdeš na pivo
    b = nebudeš pít alkohol
    c = půjdeš spát""")

    volba = input()
    if volba == "a":
        spatne()
    elif volba == "bc" or "cb" or "b,c" or "c,b":
       dobre()

def spatne():
    print("""Nemůžeš jít na pivo před zápasem, to je blbost.""")
def dobre():
    print("""Postoupil jsi dále, výborně.
Co uděláš, když ti zazvoní v sobotu v 8 ráno budík na zápas?
   
    a = Vypneš budík a spíš dál.
    b = Vstaneš plný energie.
    c = Posuneš si budík o 20 minut.""")

    volba = input()
    if volba == "a":
        spis_dal()
    elif volba == "b":
        vstanes()
    elif volba == "c":
        posunes()

def spis_dal():
    print("""Celý tým na tebe spoléhá a ty spíš dál.
Zklamal jsi je.""")

def posunes():
    print(""" Tím, že si vstal o 20 minut déle si nestíháš udělat snídani a musíš si koupit něco po cestě.

        a = Koupím si párek v rohlíku.
        b = Zastavím se v pekárně.""")

    volba = input()
    if volba == "a":
        fuj()
    elif volba == "b":
       fronta()

def fuj():
    print("""To snad nemyslíš vážně si dát párek v rohlíku před zápasem.
Učinil jsi špatné rozhodnotí.
Hra končí!!""")

def fronta():
    print("""Učinil jsi správné rozhodnutí, ale i tak si prohrál.
V pekárně byla fronta a nestíháš začátek zápasu.""")

def vstanes():
    print("""Tvoje volba byla správná, pokus se vstát pravou nohou,
oblékni se, nasnídej se a vem si tašku.
Nezapomněl jsi něco?

        a = ANO
        b = NE""")
    volba = input()
    if volba == "a":
        ano()
    elif volba == "b":
        ne()

def ano():
    print("""Byl jsi tak blízko a zároveň tak daleko.
Gamer over!!""")

def ne():
    print("""Výborně, máš vše a můžeš vyrazit na zápas.
Už si opravdu blízko, tak to nepokaž.
Na zápas cestuješ pěšky nebo použiješ dopravní prostředek??
    
        a = pěšky
        b = dopravní prostředek
        c = nevím""")

    volba = input()
    if volba == "a" or "b" or "c":
        vsechno()


def vsechno():
    print("""Zkouším tě nachytat, v tomto případě uznávám všechny tři odpovědi.
Nebylo řečeno ani jak to je daleko, jaké je počasí atd...
Gratuluji vítězíš v mé hře!!""")

start ()