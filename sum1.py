# Napiste funkci, ktera na vstupu dostane libovolne velky neprazdny seznam cisel
# libovolne velke mnozstvi cisel (ne jako seznam, ale jako jednotlive parametry)
# vrati jejich soucet

def sum1(seznam):
    sum = 0
    for i in seznam:
        sum = sum + i
    return sum

print (sum1([1, 5, 8]))