# Vytvorte si seznam, ktere bude uvnitr obsahovat nekolik slovniku. Kazdy slovnik bude obsahovat 3 klice a 3 hodnoty (brand => Ford, engine => 1.0, price => 300000, percent=0.4).
# Takhle vytvorte alespon 3 slovniky se tremi druhy aut a rozdilnymi hodnotami.
# Pote iterujte prvni seznamem a uvnitr cyklu iterujte klicem i hodnotou jednotlivych slovniku.
# Pro kazdy automobil zvyšte cenu o procento, ktere se nachazi pod klicem percent
# Vysledne slovniky vypiste se zmenenymi hodnotami

seznam = []

slovnik1 = {"brand": "seat", "colour": "black", "price": 35000, "procento": 0.5}
slovnik2 = {"brand": "skoda", "colour": "red", "price": 75000, "procento": 0.0}
slovnik3 = {"brand": "ford", "colour": "white", "price": 80000, "procento": 1.5}

seznam.append (slovnik1)
seznam.append (slovnik2)
seznam.append (slovnik3)

for x in seznam:
    print (x)
    for key in x:
        print(x["price"])
        print(x["procento"])
        x["price"] = x["price"] + x["price"]*x["procento"]/100
        print(x)