# book example
# book = {
#    'name': 'Programming with Python 3',
#    'pages': 157,
#    'read': True
# }

bookshelf = [False] * 10  # = [False, False, False, False, False, False, False, False, False, False]


# (0) this function will return False, if there is a book at specified "position" in bookshelf. If there is none, return True (True → there is no book, the space is free)
# position = index (0 - 9)
# this function is not graded, but is used in other functions
def is_free(position):
    return not bookshelf[position]



# (1) this function will add a book with "name", number of pages "pages" and whether the book was read "read"
# if there already is a book at given "position", this function does nothing
# use the function "is_free"
def add_book(position, name, pages, read=False):
    if isFree(position):
        bookshelf[position] = {
            "name": name,
            "pages": pages,
            "read": read
        }


# (2) returns False or index of the book with a given "name"
def find_book(name):
    for book in bookshelf:
        if book:
            if book["name"] == name:
                return bookshelf.index(book)

# (3) this function uses the "find_book" function and if there is a book with a given "name" in the "bookshelf", it changes its "read" value to True
def read_book(name):
    pass

# (4) this function removes all books that were read from the "bookshelf" (read == True) (replaces them with False)
def remove_read_books():
    pass

# (5) returns a list with names of books in the "bookshelf"
def names_of_books():
    pass

# BONUS: returns the average number of pages of books in the "bookshelf"
def average_number_of_pages():
    pass

'''
# testing

# 1
add_book(0, 'A Byte of python', 120, read = True)
add_book(1, 'Automate the boring stuff with Python', 420)
add_book(2, 'Python for Informatics', 500, read = True)
add_book(3, 'Learning Geospatial Analysis with Python', 155)
add_book(4, 'Python Geospatial Development', 211)
add_book(5, 'Python for Data Analysis', 362, read = True)
add_book(5, 'Programming Java', 874)

print(is_free(5)) # False
print(is_free(6)) # True


# 2
print(find_book('A Byte of python')) # 0
print(find_book('Python Geospatial Development')) # 4
print(find_book('Programming Java')) # False

# 3
read_book('A Byte of python')
read_book('Automate the boring stuff with Python')
read_book('Programming Java')


# 4
remove_read_books()
print(is_free(1)) # True
print(is_free(3)) # False


# 5
print(names_of_books()) # ['Learning Geospatial Analysis with Python', 'Python Geospatial Development']


# BONUS
print(average_number_of_pages()) # 183


# HINT
print(bookshelf)
# [False, False, False, {'pages': 155, 'name': 'Learning Geospatial Analysis with Python', 'read': False}, {'pages': 211, 'name': 'Python Geospatial Development', 'read': False}, False, False, False, False, False]

'''
